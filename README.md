# xetra-helm

Helm chart for xetra aggregator

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Config


create ~/.aws/empty_user.yaml
```
aws_secret_key: <YOUR_SECRET_KEY>
aws_access_key: <YOUR_ACCESS_KEY>
```

## install

```
helm install -f ~/.aws/empty_user.yaml -f xetra-stats/values.yaml xetra xetra-stats
helm upgrade -f ~/.aws/empty_user.yaml -f xetra-stats/values.yaml xetra xetra-stats
```
