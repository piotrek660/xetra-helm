https://helm.sh/docs/chart_template_guide/subcharts_and_globals/

# simulate

to generate the chart, without executing on K8S, run with --dry-run:

```
helm install --generate-name --dry-run --debug mychart
```


```
helm upgrade --install --atomic clunky-serval ./mychart --dry-run --debug --set favoriteDrink=milk
```


# get all info

```
helm get manifest clunky-serval
```
